<?php
/*
Plugin Name: (Sub-)Category dropdown
Plugin URI: https://gitlab.com/yalit/wpPlugin_subcatList
Description: Un plugin listant dans une liste déroulante les enfants d'une catégorie
Version: 0.1
Author: Yalit
Author URI: http://yalit.be
License: GPL2
*/

//Widget load
function wpb_load_widget() {
    register_widget( 'subcatlist_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

class subcatlist_widget extends WP_Widget {
 
  function __construct() {
    parent::__construct(

    // Base ID of your widget
    'subcatlist_widget', 

    // Widget name will appear in UI
    __('Sub-Category Dropdown', 'subcatlist_widget_domain'), 

    // Widget description
    array( 'description' => __( "Plugin permettant de lister dans une liste déroulante les enfants d'une catégorie" ), ) 
    );
    
    function subcatlist_widget()
    {
      
    }
  }
  
  // Widget Backend 
  public function form( $instance ) {
    $defaults = array( 'title' => '', 'parent_slug' => '' ,'display_count' => 'off' );
    $instance = wp_parse_args( ( array ) $instance, $defaults );

    // Widget admin form
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'parent_slug' ); ?>"><?php _e( 'Slug de la Catégorie parente:' ); ?></label> 
      <input class="widefat" id="<?php echo $this->get_field_id( 'parent_slug' ); ?>" name="<?php echo $this->get_field_name( 'parent_slug' ); ?>" type="text" value="<?php echo esc_attr( $instance['parent_slug'] ); ?>" />
    </p>
    <p>
      <input type="checkbox" <?php checked( $instance[ 'display_count' ], 'on' ); ?> id="<?php echo $this->get_field_id('display_count'); ?>" name="<?php echo $this->get_field_name('display_count'); ?>" />
      <label for="<?php echo $this->get_field_id('display_count'); ?>"><?php _e( 'Montrer le nombre de posts' ) ?></label>
    </p>
    <?php 
  }

  public function widget($args, $instance)
  {
    echo $args['before_widget'];
    echo $args['before_title'];
    echo apply_filters('widget_title', $instance['title']);
    echo $args['after_title'];

    if(isset($instance['parent_slug'])){
      $terms = get_terms(array('taxonomy'=>'category', 'slug' => $instance['parent_slug']) );
      $authors = get_terms(array('taxonomy'=>'category', 'child_of' => $terms[0]->term_id));
      
      echo '<label class="select_label">';
        echo '<select id="subactlist_select" class="subcatlist_select_class" style="width:100%; padding: 5px; border-radius: 3px;" onchange="(window.location = this.options[this.selectedIndex].value)" >';
          echo '<option id="subactlist_select_option_" class="subcatlist_select_option_class" value="">Choisir...</option>';
          foreach($authors as $author){
            $author_count = ($instance['display_count']=='on')?'('.$author->count.')':'';
            echo '<option id="subactlist_select_option_'.$author->term_id.'" class="subcatlist_select_option_class" value="'.esc_url(get_category_link($author->term_id)).'">'.$author->name.' '.$author_count.'</option>';
          }
        echo '</select>';
      echo '</label>';
    }  

    echo $args['after_widget'];
  }
  
}

